package com.example.joke.activities

import android.text.TextUtils

 object  StringUtils {

     private fun isEmpty(text: String?): Boolean {
        return if (text == null || text.equals("null", ignoreCase = true) || text.equals(
                "",
                ignoreCase = true
            )
        ) {
            true
        } else TextUtils.isEmpty(text.trim { it <= ' ' })
    }

    fun isEmpty(text: CharSequence?): Boolean {
        return if (text == null) {
            true
        } else isEmpty(text.toString())
    }
}