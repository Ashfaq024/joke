package com.example.joke.activities

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.joke.R
import com.example.joke.fragments.AddJoke
import com.example.joke.fragments.NewJokes
import com.example.joke.interfaces.onPostClickListener
import com.example.joke.model.PostModel


class MainActivity : AppCompatActivity() , onPostClickListener {
    private var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragment = NewJokes()
        loadFragment(fragment)
       /* navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_chats -> {
                    fragment = NewJokes()
                    loadFragment(fragment)
                }
                R.id.navigation_post -> {
                    fragment = AddJoke()
                    loadFragment(fragment)
                }

            }
            true
        }

*/
    }

    private fun loadFragment(fragment: Fragment?) {
        // load fragment
        val transaction = supportFragmentManager.beginTransaction()
        fragment?.let { transaction.replace(R.id.frame_container, it) }
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        fragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun cardClicked(model: PostModel?, position: Int) {
        (fragment as NewJokes).cardClicked(model ,position)
    }

    override fun onSharedIntent(model: PostModel?, position: Int) {
        (fragment as NewJokes).onSharedIntent(model ,position)
    }

}