package com.example.joke.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.*
import android.location.Address
import android.location.LocationListener
import android.os.*
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.joke.App
import com.example.joke.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FetchPlaceResponse
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.navigation.NavigationView
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import okhttp3.*
import org.json.JSONObject
import org.w3c.dom.Document
import org.xml.sax.SAXException
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException
import kotlin.collections.ArrayList


class LocationTracking : FragmentActivity(), LocationListener, OnMapReadyCallback,
    GoogleMap.OnInfoWindowClickListener,
    NavigationView.OnNavigationItemSelectedListener {
    private val tripid: String? = "4901"
    private val mobileno: String = "03215857799"
    private var firstTimeFlag: Boolean = true
    private var gpsService: BackgroundLocationService? = null
    private var storageReference: StorageReference? = null
    var mDatabase: DatabaseReference? = null
    private var placesClient: PlacesClient? = null
    var place: Place? = null

    var md: GMapV2Direction = GMapV2Direction()

    private var pickUpLocationID: String? = null
    private var pickUpLocationAddress: String? = null
    private var progress_main: ProgressBar? = null
    var searchblock: LinearLayout? = null
    private var drawer: DrawerLayout? = null
    private var mapView: View? = null
    var arrivedMarker: Marker? = null
    var markerPoints: ArrayList<LatLng?>? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: Int = 10
    private var locationPermissionGranted: Boolean = false
    private var btnDropOffLayout: LinearLayout? = null
    private var btndropoff: AppCompatButton? = null
    private var main: RelativeLayout? = null
    private var btnProceed: AppCompatButton? = null
    private var animFadein: Animation? = null
    private var progress: ProgressBar? = null
    private var progress2: ProgressBar? = null
    private var mapFragment: SupportMapFragment? = null
    private var isDropoff: Boolean = false
    private var isAlreadyPlotted: Boolean = false
    private var mCenterMarker: Marker? = null
    private var address: String? = "fetching Location.."
    private var lati: Double = 0.0
    private var longi: Double = 0.0
    private var googleMap: GoogleMap? = null
    private var locationManager: LocationManager? = null

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                }
            }
        }
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0L,
            0f,
            this
        )
        locationManager?.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            0L,
            0f,
            this
        )

    }


    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true

        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.activity_location_tracking)

        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        drawer = findViewById(R.id.drawer_layout)
        progress = findViewById(R.id.progressBar)
        progress_main = findViewById(R.id.progress)
        progress2 = findViewById(R.id.progressBar2)
        animFadein =
            AnimationUtils.loadAnimation(applicationContext, R.anim.slidelefttoright)

        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        btnProceed = findViewById(R.id.btnProceed)
        btndropoff = findViewById(R.id.btndropoff)
        searchblock = findViewById(R.id.searchblock)
        btnDropOffLayout = findViewById(R.id.btnDropOffLayout)
        main = findViewById(R.id.main)
        mapFragment?.getMapAsync(this)
        drawer?.setOnClickListener {
            drawer?.closeDrawer(GravityCompat.END)
        }

        Places.initialize(this, "AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM")


        placesClient = Places.createClient(this)
        val ref = FirebaseDatabase.getInstance()
        mDatabase = FirebaseDatabase.getInstance().reference
        val storage =
            FirebaseStorage.getInstance().getReferenceFromUrl("gs://joke-99612.appspot.com")
        storageReference = storage.root
        mDatabase = FirebaseDatabase.getInstance().reference
        markerPoints = ArrayList()

        ref.getReference("online_drivers").child(mobileno)
            .addValueEventListener(object : ValueEventListener {

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (!snapshot.exists()) {
                        return
                    }
                    lati = snapshot.child("lat").value as Double
                    longi = snapshot.child("lng").value as Double
                    println(LatLng(lati, longi));
                    markerPoints?.add(LatLng(lati, longi))
                    address = Geocode(lati, longi)
                    //   googleMap?.clear()
                    if (mCenterMarker == null) {


                        mCenterMarker = googleMap?.addMarker(
                            markerPoints?.get(0)?.latitude?.let {
                                LatLng(
                                    it,
                                    markerPoints!![0]!!.longitude
                                )
                            }?.let {
                                MarkerOptions()
                                    .position(it)
                                    .draggable(true)
                                    .flat(true)
                                    .icon(
                                        bitmapDescriptorFromVector(
                                            this@LocationTracking,
                                            R.drawable.ic_my_current_location
                                        )
                                    )
                            })
                    } else {
                        mCenterMarker?.position = googleMap?.cameraPosition?.target
                    }
                    findViewById<TextView>(R.id.txtLocationTitle).text = address
                    findViewById<TextView>(R.id.txtLocationTitle).startAnimation(animFadein)
//                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(lati, longi), 15F)
//                googleMap?.animateCamera(cameraUpdate)
                    googleMap?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(lati, longi),
                            15f
                        )
                    );
                }

                override fun onCancelled(error: DatabaseError) {
                }
            })

        /* if (!isMyServiceRunning(MyService::class.java)) {
             val intent = Intent(this, BackgroundLocationService::class.java)
             this.startService(intent)
             gpsService?.startTracking()
         }*/
    }

    override fun onLocationChanged(p0: Location) {
        lati = p0.latitude
        longi = p0.longitude

    }

    override fun onStart() {
        super.onStart()

    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val name = className.className
            if (name.endsWith("BackgroundLocationService")) {
                gpsService = (service as BackgroundLocationService.LocationServiceBinder).service
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            if (className.className == "BackgroundLocationService") {
                gpsService = null
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        progress?.visibility = View.VISIBLE
        googleMap?.clear()
        getLocationPermission()
        getLocation()
        if (googleMap != null) {
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL;
            googleMap.uiSettings?.isZoomGesturesEnabled = true;
            googleMap.uiSettings?.isCompassEnabled = true;
            googleMap.uiSettings?.isMyLocationButtonEnabled = true;
        }
        googleMap?.setMaxZoomPreference(18f)
        updateLocation()
        /*   googleMap?.uiSettings?.isRotateGesturesEnabled = true;
           googleMap?.uiSettings?.isScrollGesturesEnabled = true;
           googleMap?.uiSettings?.isTiltGesturesEnabled = true;
           googleMap?.uiSettings?.isZoomGesturesEnabled = true;
           googleMap?.uiSettings?.isMapToolbarEnabled = true;
           googleMap?.isTrafficEnabled = true*/
        //  googleMap?.setOnInfoWindowClickListener(this);

        mCenterMarker = googleMap?.addMarker(
            MarkerOptions()
                .position(googleMap.cameraPosition.target)
                .draggable(true)
                .flat(true)
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_my_current_location))
        )

        if (mCenterMarker != null && address == null) {
            address = "fetching Location.."

            address = Geocode(
                googleMap?.cameraPosition?.target!!.latitude,
                googleMap.cameraPosition?.target!!.latitude
            )

            //mCenterMarker?.showInfoWindow()
        }
        findViewById<TextView>(R.id.txtLocationTitle).text = address
        findViewById<TextView>(R.id.txtLocationTitle).startAnimation(animFadein)

        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(lati, longi), 18F)
        googleMap?.animateCamera(cameraUpdate)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lati, longi), 18f));
        googleMap?.setOnCameraIdleListener {
            if (isDropoff) {
                isAlreadyPlotted = true
                mCenterMarker?.position = googleMap.cameraPosition.target
                address = Geocode(
                    googleMap.cameraPosition.target.latitude,
                    googleMap.cameraPosition.target.longitude
                )

                if (lati != 0.0 && longi != 0.0) {
                    mCenterMarker?.snippet = address

                    findViewById<TextView>(R.id.txtDropOffLocation).text = address
                    findViewById<TextView>(R.id.txtDropOffLocation).startAnimation(animFadein)

                }
                progress2?.visibility = View.INVISIBLE
            } else {
                isAlreadyPlotted = true
                mCenterMarker?.position = googleMap.cameraPosition.target
                address = Geocode(
                    googleMap.cameraPosition.target.latitude,
                    googleMap.cameraPosition.target.longitude
                )

                if (lati != 0.0 && longi != 0.0) {
                    mCenterMarker?.snippet = address

                    findViewById<TextView>(R.id.txtLocationTitle).text = address
                    findViewById<TextView>(R.id.txtLocationTitle).startAnimation(animFadein)

                }
                progress?.visibility = View.INVISIBLE
            }
        }

        googleMap!!.setOnCameraMoveListener {
            if (isDropoff) {
                progress2?.visibility = View.VISIBLE
            } else {
                progress?.visibility = View.VISIBLE

            }

            if (isAlreadyPlotted) {

                val midLatLng = googleMap.cameraPosition.target
                if (mCenterMarker != null && midLatLng != null) {
                    mCenterMarker!!.position = midLatLng
                }
            }
        }
        googleMap.setOnCameraMoveStartedListener {
            if (isDropoff) {
                progress2?.visibility = View.VISIBLE
            } else {
                progress?.visibility = View.VISIBLE

            }
            googleMap.setInfoWindowAdapter(CustomInfoWindowForGoogleMap(this))

        }

        googleMap.setOnMapClickListener {
            mCenterMarker?.position = googleMap.cameraPosition.target
            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        it.latitude,
                        it.longitude
                    ), 15.0f
                )
            )
        }
        googleMap.setOnMapLoadedCallback(OnMapLoadedCallback {
            Log.e(
                "TAG",
                googleMap.cameraPosition.target.toString()
            )
            mCenterMarker?.position = googleMap.cameraPosition.target
            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        googleMap.cameraPosition.target.latitude,
                        googleMap.cameraPosition.target.longitude
                    ), 15.0f
                )
            )

        })
        googleMap.uiSettings.isMyLocationButtonEnabled = true;

        googleMap.setOnMyLocationClickListener {
            mCenterMarker?.position = googleMap.cameraPosition.target
            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        it.latitude,
                        it.longitude
                    ), 15.0f
                )
            )
        }
    }

    private fun updateLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0L,
            0f,
            this
        )
        locationManager?.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            0L,
            0f,
            this
        )

    }

    fun bitmapDescriptorFromVector(
        context: Context,
        vectorResId: Int
    ): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(
                intrinsicWidth,
                intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    fun Geocode(latitude: Double, longitude: Double): String {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(this, Locale.getDefault())
        try {

            addresses = geocoder.getFromLocation(
                latitude,
                longitude,
                1
            )
            if (addresses.isNotEmpty()) {
                address = addresses[0].getAddressLine(0)
            } else {
                return "$address"
            }
            if (addresses[0].getAddressLine(0) == "null") {
                address = "Fetching location..."
            }
        } catch (E: java.lang.Exception) {
        }




        return "$address"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.navigation_chats -> {

                val LicenseDialog: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(this)
                LicenseDialog.setTitle("Legal Notices")
                LicenseDialog.setMessage("LicenseInfo")
                LicenseDialog.show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onInfoWindowClick(p0: Marker?) {
        p0?.showInfoWindow()
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)
        return if (ConnectionResult.SUCCESS == status) {
            true
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show()
            false
        }
    }

    fun onclickpickup(view: View) {
        isDropoff = true
        findViewById<View>(R.id.directionView).visibility = View.VISIBLE
        findViewById<View>(R.id.directionView).visibility = View.VISIBLE
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(lati.let {
            LatLng(
                it, longi
            )
        }, 15f));
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(lati.let {
            LatLng(
                it, longi
            )
        }, 15f));
        //address = Geocode(lati, longi)
        btnProceed?.visibility = View.GONE
        btndropoff?.visibility = View.VISIBLE
        findViewById<TextView>(R.id.txtDropOffLocation).text = address
        findViewById<TextView>(R.id.txtDropOffLocation).startAnimation(animFadein)
        btnDropOffLayout?.visibility = View.VISIBLE


    }

    private val locationCallBack: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val location: Location? = locationResult.lastLocation
            if (firstTimeFlag && googleMap != null) {
                // onMapReady(googleMap)
                firstTimeFlag = false;
            }
            if (location != null) {
                lati = location.latitude
                longi = location.longitude
                stopLocationUpdates()
            }

        }
    }


    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val locationManager: LocationManager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                    LocationManager.NETWORK_PROVIDER
                )
            ) {
                fusedLocationProviderClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null)
                        startCurrentLocationUpdates()
                    else {
                        lati = location.latitude
                        longi = location.longitude
                        val myloc = LatLng(lati, longi)
                        val update = CameraUpdateFactory.newLatLngZoom(myloc, 15f)
                        mCenterMarker?.position = myloc
                        googleMap?.animateCamera(update)
                        googleMap?.moveCamera(update)
                        Geocode(lati, longi)
                        main?.visibility = View.VISIBLE
                        btnProceed?.visibility = View.VISIBLE
                        btnProceed?.startAnimation(animFadein)
                        main?.startAnimation(animFadein)
                        stopLocationUpdates()

                    }
                }
            }
        }
    }


    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String? {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/json?$parameters&key=AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM"
    }


    // Fetches data from url passed
    internal class DownloadTask(
        var googleMap: GoogleMap,
        var arrivedMarker: Marker?,
        var markerPoints: ArrayList<LatLng?>,
        var searchblock: LinearLayout?,
        var progress_main: ProgressBar?
    ) :
        AsyncTask<String?, Void?, String>() {
        // Downloading data in non-ui thread
        protected override fun doInBackground(vararg url: String?): String {

            // For storing data from web service
            var data = ""
            try {
                // Fetching the data from web service
                data = url[0]?.let { downloadUrl(it) }!!
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }
            return data
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        override fun onPostExecute(result: String) {
            super.onPostExecute(result)

            val parserTask =
                ParserTask(googleMap, arrivedMarker, markerPoints, searchblock, progress_main)

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result)
        }

        /**
         * A method to download json data from url
         */
        @SuppressLint("LongLogTag")
        @Throws(IOException::class)
        fun downloadUrl(strUrl: String): String {
            var data = ""
            var iStream: InputStream? = null
            var urlConnection: HttpURLConnection? = null
            try {
                val url = URL(strUrl)

                // Creating an http connection to communicate with url
                urlConnection = url.openConnection() as HttpURLConnection

                // Connecting to url
                urlConnection.connect()

                // Reading data from url
                iStream = urlConnection.getInputStream()
                val br = BufferedReader(InputStreamReader(iStream))
                val sb = StringBuffer()
                var line: String? = ""
                while (br.readLine().also { line = it } != null) {
                    sb.append(line)
                }
                data = sb.toString()
                br.close()
            } catch (e: Exception) {
                Log.d("Exception while downloading url", e.toString())
            } finally {
                iStream?.close()
                urlConnection?.disconnect()
            }
            return data
        }

    }

    class ParserTask(
        val googleMap: GoogleMap?,
        var arrivedMarker: Marker?,
        var markerPoints: ArrayList<LatLng?>,
        var searchblock: LinearLayout?,
        var progress_main: ProgressBar?
    ) :
        AsyncTask<String?, Int?, List<List<HashMap<String, String>>>?>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String?): List<List<HashMap<String, String>>>? {
            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null
            try {
                jObject = JSONObject(jsonData[0])
                val parser = DirectionsJSONParser()

                // Starts parsing data
                routes = parser.parse(jObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return routes
        }

        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>?) {
            var points: ArrayList<LatLng?>? = null
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()
            var distance: String? = ""
            var duration: String? = ""
            for (i in 0 until result?.size!!) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                val path = result[i]

                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    if (j == 0) {    // Get distance from the list
                        distance = point["distance"]
                        continue
                    } else if (j == 1) { // Get duration from the list
                        duration = point["duration"]
                        continue
                    }
                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    val position = LatLng(lat, lng)
                    points.add(position)
                }
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(Color.parseColor("#000000"))
                lineOptions.endCap(RoundCap())
                lineOptions.jointType(JointType.ROUND)
            }
            arrivedMarker = googleMap?.addMarker(
                markerPoints[0]?.latitude?.let {
                    LatLng(
                        it,
                        markerPoints[0]!!.longitude
                    )
                }?.let {
                    MarkerOptions()
                        .position(it)
                        .draggable(false)
                        .flat(true)
                }
            )
            arrivedMarker?.title = "Arrived by\n"
            try {
                arrivedMarker?.snippet = getTime(duration)
                arrivedMarker?.showInfoWindow()

            } catch (E: Exception) {
                E.message
            }

            val builder = LatLngBounds.Builder()
            builder.include(LatLng(markerPoints[0]!!.latitude, markerPoints[0]!!.longitude))
            builder.include(LatLng(markerPoints[1]!!.latitude, markerPoints[1]!!.longitude))
            val bound = builder.build()
            googleMap?.addPolyline(lineOptions)
            googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 30))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 30), 50, null)
            googleMap.uiSettings.setAllGesturesEnabled(true);
            progress_main?.visibility = View.GONE
            //  isAlreadyPlotted = true
            Log.e("duration", duration.toString())
            //    tvDistanceDuration.setText("Distance:$distance, Duration:$duration")

        }

        @SuppressLint("SimpleDateFormat")
        private fun getTime(duration: String?): String? {
            if (duration.isNullOrEmpty()) {
                val now = Calendar.getInstance()
                now.add(Calendar.MINUTE, 5)
                val df = SimpleDateFormat("hh:mm aa")
                return df.format(now.time);
            } else {
                val min = duration?.substring(0, duration.length - 5).toInt()
                val now = Calendar.getInstance()
                min.let { now.add(Calendar.MINUTE, it) }
                val df = SimpleDateFormat("hh:mm aa")
                return df.format(now.time);
            }
        }
    }

    fun onclickdropoff(view: View) {
        searchblock?.visibility = View.GONE

        progress_main?.visibility = View.VISIBLE
        App.isDropoff = true
        App.isPickup = false
        markerPoints = ArrayList()
        markerPoints?.add(LatLng(lati, longi))
        markerPoints?.add(googleMap?.cameraPosition?.target?.latitude?.let {
            LatLng(
                it,
                googleMap?.cameraPosition?.target?.longitude!!
            )
        })
        val origin: LatLng = markerPoints?.get(0)!!
        val dest: LatLng = markerPoints?.get(1)!!
        googleMap?.clear()
        googleMap?.addMarker(
            markerPoints?.get(0)?.latitude?.let {
                LatLng(
                    it,
                    markerPoints!![0]!!.longitude
                )
            }?.let {
                MarkerOptions()
                    .position(it)
                    .draggable(true)
                    .flat(true)
                    .icon(
                        bitmapDescriptorFromVector(
                            this,
                            R.drawable.ic_my_current_location
                        )
                    )
            }
        )


        // Getting URL to the Google Directions API
        val url: String = getDirectionsUrl(origin, dest)!!
        val downloadTask =
            googleMap?.let {
                DownloadTask(
                    it,
                    arrivedMarker,
                    markerPoints!!,
                    searchblock,
                    progress_main
                )
            }

        // Start downloading json data from Google Directions API
        downloadTask?.execute(url)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        fun onNavigationItemSelected(item: MenuItem): Boolean {
            // Handle navigation view item clicks here.
            val id = item.itemId
            if (id == R.id.nav_camera) {
                // Handle the camera action
            } else if (id == R.id.nav_gallery) {
            } else if (id == R.id.nav_slideshow) {
            } else if (id == R.id.nav_manage) {
            } else if (id == R.id.nav_share) {
            } else if (id == R.id.nav_send) {
            }
            drawer?.closeDrawer(GravityCompat.START)
            return true
        }
        return false
    }

    override fun onBackPressed() {
        isDropoff = false
        App.isDropoff = false
        App.isPickup = true
        btnProceed?.visibility = View.VISIBLE
        btndropoff?.visibility = View.GONE
        btnDropOffLayout?.visibility = View.GONE
        searchblock?.visibility = View.VISIBLE
        drawer?.closeDrawer(GravityCompat.END)

        /* googleMap?.clear()
         if (googleMap != null) {
             mapFragment?.getMapAsync(this)

         }*/
        findViewById<View>(R.id.directionView).visibility = View.GONE
        findViewById<TextView>(R.id.txtDropOffLocation).text = ""
        if (drawer?.isDrawerOpen(GravityCompat.START)!!) {
            drawer?.closeDrawer(GravityCompat.START)
        } /*else {
            super.onBackPressed()
        }*/
    }

    fun onclose(view: View) {
        drawer?.closeDrawer(GravityCompat.END)
        finish()

    }

    fun onbtnclick(view: View) {
        drawer?.openDrawer(GravityCompat.END)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String) {}

    override fun onProviderDisabled(provider: String) {}
    fun onclick_empty(view: View) {
        view.visibility = View.VISIBLE
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            pickUpLocationID = data?.getStringExtra("pickUpCityID")
            pickUpLocationAddress = data?.getStringExtra("pickUpLocationAddress")
            if (!StringUtils.isEmpty(pickUpLocationID)) {
                pickUpLocationID?.let { createPathBetweenTwoPoints(pickUpLocationID, it) }
            }

        }
        if (requestCode == 11) {
            pickUpLocationID = data?.getStringExtra("pickUpCityID")
            pickUpLocationAddress = data?.getStringExtra("pickUpLocationAddress")
            //  var lat_ = data?.getStringExtra("lat")
            //var long_ = data?.getStringExtra("long")
            if (!StringUtils.isEmpty(pickUpLocationID)) {
                pickUpLocationID?.let {
                    setmarker(
                        pickUpLocationID!!,
                        it,
                        pickUpLocationAddress
                    )
                }
            }
        }

    }

    private fun setmarker(
        pickUpLocationID: String,
        it: String,
        pickUpLocationAddress: String?
    ) {
        val latitude = App.lati
        val longitude = App.longi
        val newLocation = latitude.let { it1 -> longitude?.let { it2 -> LatLng(it1, it2) } }
        markerPoints = ArrayList()

        address = latitude?.let { it1 -> longitude?.let { it2 -> Geocode(it1, it2) } }
        findViewById<TextView>(R.id.txtLocationTitle).text = address
        findViewById<TextView>(R.id.txtLocationTitle).startAnimation(animFadein)


        val placeFields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG)

        val request = FetchPlaceRequest.builder(pickUpLocationID, placeFields).build()

        placesClient!!.fetchPlace(request).addOnSuccessListener { response: FetchPlaceResponse ->
            val place = response.place
            Log.i("TAG", "Place found: " + place.name)
            Log.i("TAG", "Place found: " + place.latLng)
            markerPoints?.add(place.latLng)
            googleMap?.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    place.latLng,
                    15f
                )
            );
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.latLng, 15F)
            googleMap?.animateCamera(cameraUpdate)
            mCenterMarker?.position = place.latLng
            address = place.name
        }.addOnFailureListener { exception: java.lang.Exception ->
            if (exception is ApiException) {
                val apiException =
                    exception as ApiException
                val statusCode = apiException.statusCode
                // Handle error with given status code.
                Log.e("TAG", "Place not found: " + exception.message)
            }
        }

    }

    private fun createPathBetweenTwoPoints(pickUpLocationID: String?, dropOffLocationID: Any) {
        val context = GeoApiContext.Builder()
            .apiKey("AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM")
            .build()
        markerPoints = ArrayList()
        markerPoints?.add(
            LatLng(
                googleMap!!.cameraPosition.target.latitude,
                googleMap!!.cameraPosition.target.longitude
            )
        )
        val results = GeocodingApi.newRequest(context)
            .latlng(
                com.google.maps.model.LatLng(
                    googleMap!!.cameraPosition.target.latitude,
                    googleMap!!.cameraPosition.target.longitude
                )
            )
            .await()

        val url = ("https://maps.googleapis.com/maps/api/directions/xml?"
                + "origin=place_id:"
                + results[0].placeId
                + "&destination=place_id:"
                + pickUpLocationID
                + "&sensor=false&units=metric&mode=driving&key=AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM")
        GetDirection(url)
        /*val downloadTask =
            googleMap?.let {
                DownloadTask(
                    it,
                    arrivedMarker,
                    markerPoints!!,
                    searchblock,
                    progress_main
                )
            }

        downloadTask?.execute(url)*/
    }

    fun GetDirection(url: String?) {
        progress_main?.visibility = View.VISIBLE
        Log.e("Url", url!!)
        try {
            val client = OkHttpClient()
            val request: Request = okhttp3.Request.Builder()
                .url(url)
                .build()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        progress_main?.visibility = View.GONE

                        Toast.makeText(
                            this@LocationTracking,
                            "Unable to get location detail. Please try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val `is` = response.body!!.byteStream()
                    try {
                        val builder = DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                        val doc = builder.parse(`is`)
                        if (doc != null) {
                            ParseData(doc)
                        } else {
                            ParseData(null)
                        }
                    } catch (e: SAXException) {
                        e.printStackTrace()
                    } catch (e: ParserConfigurationException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        Log.e("URL of location", url)
    }

    fun ParseData(result: Document?) {

        runOnUiThread {
            progress_main?.visibility = View.GONE

            if (result == null) {
                Toast.makeText(
                    this@LocationTracking,
                    "Unable to get location detail.", Toast.LENGTH_LONG
                )
                    .show()
            } else {
                val directionPoint: java.util.ArrayList<LatLng> =
                    md.getDirection(result)
                if (directionPoint.size > 0) {
                    val rectLine = PolylineOptions().width(12f)
                        .color(Color.parseColor("#0000b3"))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint[i])
                    }

                    googleMap!!.clear()
                    val sourceLatLng = directionPoint[0]
                    val destinationLatLng = directionPoint[directionPoint.size - 1]
                    googleMap!!.addPolyline(rectLine)
                    var pickUpLatitude = sourceLatLng.latitude
                    var pickUpLongitude = sourceLatLng.longitude
                    var dropOffLatitude = destinationLatLng.latitude
                    var dropOffLongitude = destinationLatLng.longitude
                    val totalDistance: String = md.getDistanceText(result)
                    googleMap!!.addMarker(
                        MarkerOptions()
                            .position(sourceLatLng)
                            .title(md.getStartAddress(result))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_current_location))
                    )
                    mCenterMarker?.position = LatLng(
                        googleMap?.cameraPosition!!.target.latitude,
                        googleMap?.cameraPosition!!.target.latitude
                    )
                    mCenterMarker?.showInfoWindow()
                    try {
                        val TP1 = googleMap!!.addMarker(
                            MarkerOptions()
                                .position(destinationLatLng)
                                .title(
                                    "Arrived by\n" +
                                            getTime(md.getDurationText(result))
                                )
                            //  .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_current_location))
                        )
                        TP1.showInfoWindow()
                    } catch (e: java.lang.Exception) {
                        e.message
                    }


                    val builder = LatLngBounds.Builder()
                    builder.include(sourceLatLng)
                    builder.include(destinationLatLng)
                    val bound = builder.build()
                    googleMap!!.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(bound, 100),
                        15,
                        null
                    )
                } else {
                    Toast.makeText(
                        this@LocationTracking,
                        "Unable to get places detail.", Toast.LENGTH_LONG
                    )
                        .show()
                }
            }
            googleMap?.uiSettings?.setAllGesturesEnabled(true);
            progress_main?.visibility = View.GONE
            searchblock?.visibility = View.GONE

            val mBottomSheetDialog = RoundedBottomSheetDialog(this)
            val sheetView = layoutInflater.inflate(R.layout.dialog_2_my_rounded_bottom_sheet, null)
            mBottomSheetDialog.setContentView(sheetView)

            mBottomSheetDialog.show()
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getTime(duration: String?): String? {
        if (duration.isNullOrEmpty()) {
            val now = Calendar.getInstance()
            now.add(Calendar.MINUTE, 5)
            val df = SimpleDateFormat("hh:mm aa")
            return df.format(now.time);
        } else {
            if (StringUtils.isEmpty(duration)) {
                val now = Calendar.getInstance()
                now.add(Calendar.MINUTE, 5)
                val df = SimpleDateFormat("hh:mm aa")
                return df.format(now.time);
            } else {
                val min = duration.substring(0, duration.length - 5).toInt()
                val now = Calendar.getInstance()
                min.let { now.add(Calendar.MINUTE, it) }
                val df = SimpleDateFormat("hh:mm aa")
                return df.format(now.time);
            }

        }
    }


    fun onbase_picup(view: View) {
        App.isDropoff = false
        App.isPickup = true
        var intent = Intent(this@LocationTracking, LocationSearch::class.java)
        startActivityForResult(intent, 11)
    }

    fun onbase_dropoff(view: View) {
        view.visibility = View.VISIBLE
        App.isDropoff = true
        App.isPickup = false
        val intent = Intent(this@LocationTracking, LocationSearch::class.java)
        startActivityForResult(intent, 10)
    }


    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallBack)
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        if (isGooglePlayServicesAvailable()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            startCurrentLocationUpdates();
        }
    }

    override fun onStop() {
        super.onStop()
        if (fusedLocationProviderClient != null) fusedLocationProviderClient.removeLocationUpdates(
            locationCallBack
        )
    }

    private fun startCurrentLocationUpdates() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 1000
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@LocationTracking, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                )
                return
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallBack,
            Looper.myLooper()
        )
    }
}







