package com.example.joke.activities

import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.JobIntentService
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class MyService : JobIntentService() {
    private val JOB_ID: Int = 100
    private var mLocationManager: LocationManager? = null

    class LocationListener(provider: String) : android.location.LocationListener {
        private var storageReference: StorageReference? = null
        private var mDatabase: DatabaseReference? = null
        var mLastLocation: Location
        override fun onLocationChanged(location: Location) {
            Log.e(TAG, "onLocationChanged: $location")
            mLastLocation.set(location)
            mDatabase = FirebaseDatabase.getInstance().reference
            val storage =
                FirebaseStorage.getInstance().getReferenceFromUrl("gs://joke-99612.appspot.com")
            storageReference = storage.root
            mDatabase = FirebaseDatabase.getInstance().reference
            mDatabase?.child("Location")?.child("Lat")?.setValue(location.latitude)
            mDatabase?.child("Location")?.child("Lng")?.setValue(location.longitude)
        }

        override fun onProviderDisabled(provider: String) {
            Log.e(TAG, "onProviderDisabled: $provider")
        }

        override fun onProviderEnabled(provider: String) {
            Log.e(TAG, "onProviderEnabled: $provider")
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.e(TAG, "onStatusChanged: $provider")
        }

        init {
            Log.e(TAG, "LocationListener $provider")
            mLastLocation = Location(provider)
        }
    }

    var mLocationListeners = arrayOf(
        LocationListener(LocationManager.GPS_PROVIDER),
        LocationListener(LocationManager.NETWORK_PROVIDER)
    )


    override fun onHandleWork(intent: Intent) {
        try {
            mLocationManager!!.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, LOCATION_INTERVAL.toLong(), LOCATION_DISTANCE,
                mLocationListeners[0]
            )
            Toast.makeText(this,"running", Toast.LENGTH_SHORT).show()
        } catch (ex: SecurityException) {
            Log.i(TAG, "fail to request location update, ignore", ex)

        }
    }


    override fun onStopCurrentWork(): Boolean {

        return super.onStopCurrentWork()
    }

    override fun onCreate() {
        Log.e(TAG, "onCreate")
        initializeLocationManager()
        try {
            mLocationManager!!.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL.toLong(), LOCATION_DISTANCE,
                mLocationListeners[1]
            )
        } catch (ex: SecurityException) {
            Log.i(TAG, "fail to request location update, ignore", ex)
        } catch (ex: IllegalArgumentException) {
            Log.d(TAG, "network provider does not exist, " + ex.message)
        }
        try {
            mLocationManager!!.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, LOCATION_INTERVAL.toLong(), LOCATION_DISTANCE,
                mLocationListeners[0]
            )
        } catch (ex: SecurityException) {
            Log.i(TAG, "fail to request location update, ignore", ex)
        } catch (ex: IllegalArgumentException) {
            Log.d(TAG, "gps provider does not exist " + ex.message)
        }
    }


    override fun onDestroy() {
        Log.e(TAG, "onDestroy")
        super.onDestroy()
        if (mLocationManager != null) {
            for (i in mLocationListeners.indices) {
                try {
                    mLocationManager!!.removeUpdates(mLocationListeners[i])
                } catch (ex: Exception) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex)
                }
            }
        }
    }

    private fun initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager")
        if (mLocationManager == null) {
            mLocationManager =
                applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
        }
    }

    companion object {
        fun enqueueWork(context: Context?, work: Intent?) {
            if (work != null) {

                enqueueWork(context!!, MyService::class.java, 1000, work)
            }
        }


        private const val TAG = "BOOMBOOMTESTGPS"
        private const val LOCATION_INTERVAL = 1000
        private const val LOCATION_DISTANCE = 1f
    }
}