package com.example.joke.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorCompat
import com.example.joke.App
import com.example.joke.BuildConfig
import com.example.joke.LocationTrack
import com.example.joke.R
import com.example.joke.utils.SharedPrefs
import okhttp3.internal.and
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


class SplashActivity : AppCompatActivity(), LocationListener {
    private var timer: Timer? = null
    private val animationStarted = false
    private val DELAY: Long = 5000

    companion object {
        const val ANIM_ITEM_DURATION = 1000
        const val ITEM_DELAY = 300
        const val STARTUP_DELAY: Long = 0

    }

    var sb = StringBuilder()
    var stk = Stack<String>()
    var string = "i"
    var string1 = "_"
    var string2 = "l"
    var string3 = "i"
    var string4 = "k"
    var string5 = "e"
    var string6 = "_"
    var string7 = "m"
    var string8 = "y"
    var string9 = "_"
    var string10 = "c"
    var string11 = "_"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }
       // startService(Intent(this, LocationTrack::class.java))

        // KeyHelper[this]

        animate()
       // startActivity(Intent(this@SplashActivity, LocationTracking::class.java))

         launchHomeScreen()
    }

    private fun launchHomeScreen() {
        timer = Timer()
        if (!SharedPrefs.settingDone.isNullOrEmpty()) {
            timer!!.schedule(object : TimerTask() {
                override fun run() {
                    startActivity(Intent(this@SplashActivity, LocationTracking::class.java))
                    finish()
                }
            }, DELAY)
        } else {
            timer!!.schedule(object : TimerTask() {
                override fun run() {
                    startActivity(Intent(this@SplashActivity, LocationTracking::class.java))
                    finish()
                }
            }, DELAY)
        }
    }

    private fun animate() {
        val logoImageView = findViewById<View>(R.id.img) as ImageView
        val container = findViewById<View>(R.id.container) as ViewGroup
        ViewCompat.animate(logoImageView)
            .translationY(-250f)
            .setStartDelay(STARTUP_DELAY)
            .setDuration(ANIM_ITEM_DURATION.toLong()).setInterpolator(
                DecelerateInterpolator(1.2f)
            ).start()
        for (i in 0 until container.childCount) {
            val v = container.getChildAt(i)
            var viewAnimator: ViewPropertyAnimatorCompat
            viewAnimator = if (v !is Button) {
                ViewCompat.animate(v)
                    .translationY(10f).alpha(1f)
                    .setStartDelay((ITEM_DELAY * i + 500).toLong())
                    .setDuration(1000)
            } else {
                ViewCompat.animate(v)
                    .scaleY(1f).scaleX(1f)
                    .setStartDelay((ITEM_DELAY * i + 500).toLong())
                    .setDuration(500)
            }
            viewAnimator.setInterpolator(DecelerateInterpolator()).start()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (!hasFocus || animationStarted) {
            return
        }
        animate()
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    object KeyHelper {
        /**
         * @param key string like: SHA1, SHA256, MD5.
         */
        @SuppressLint("PackageManagerGetSignatures")
        operator // test purpose
        fun get(context: Context) {
            try {
                var key = "SHA256"
                val info: PackageInfo = context.getPackageManager()
                    .getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES)
                for (signature in info.signatures) {
                    val md: MessageDigest = MessageDigest.getInstance(key)
                    md.update(signature.toByteArray())
                    val digest: ByteArray = md.digest()
                    val toRet = java.lang.StringBuilder()
                    for (i in digest.indices) {
                        if (i != 0) toRet.append(":")
                        val b: Int = digest[i] and 0xff
                        val hex = Integer.toHexString(b)
                        if (hex.length == 1) toRet.append("0")
                        toRet.append(hex)
                    }
                    Log.e("KeyHelper", "$key $toRet")
                }
            } catch (e1: PackageManager.NameNotFoundException) {
                Log.e("name not found", e1.toString())
            } catch (e: NoSuchAlgorithmException) {
                Log.e("no such an algorithm", e.toString())
            } catch (e: Exception) {
                Log.e("exception", e.toString())
            }
        }
    }

    override fun onLocationChanged(p0: Location) {
        App.lati = p0.latitude
        App.longi = p0.longitude
    }
}