package com.example.joke.activities

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.example.joke.App
import com.example.joke.R
import com.example.joke.adapter.SearchLocationAdapter
import org.json.JSONObject


open class LocationSearch : AppCompatActivity() {
    private var rev: RecyclerView? = null
    private var ed1: EditText? = null
    var arraylist: ArrayList<ModelPlaceSearch>? = null
    var key: String =
        "https://maps.googleapis.com/maps/api/place/textsearch/json?query=Hassan%20Street%20Johar%20twon&location=31.506244019601258,74.33177445083857&radius=2000&key=AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM"

    var placeKey: String? = "AIzaSyDN78AHRT7AudtFcU29SGWG0gN0cYwe_DM"
    var latitude = "31.506244019601258"
    var longitude = "74.33177445083857"
    private val PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
    private val TYPE_AUTOCOMPLETE = "/autocomplete"
    private val OUT_JSON = "/json"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        supportActionBar?.show()
        supportActionBar?.title = "Search Location"
        ed1 = findViewById<EditText>(R.id.ed1);
        rev = findViewById<RecyclerView>(R.id.rec)
        ed1!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(query: CharSequence?, start: Int, before: Int, count: Int) {
                if (query?.length!! >= 1) {
                    loadNearByPlaces(query)
                }
            }
        })
    }

    private fun loadNearByPlaces(query: CharSequence?) {

        val i = intent
        val googlePlacesUrl =
            StringBuilder("https://maps.googleapis.com/maps/api/place/autocomplete/json?")
        googlePlacesUrl.append("location=").append(latitude).append(",").append(longitude)
        googlePlacesUrl.append("&input=").append(query)
        googlePlacesUrl.append("&radius=").append(25000)
        googlePlacesUrl.append("&sensor=true")
        googlePlacesUrl.append("&components=country:pk");
        googlePlacesUrl.append("&key=$placeKey")
        val request = JsonObjectRequest(googlePlacesUrl.toString(), null,
            { response ->
                Log.i("onResponse", "onResponse: Result= $response")
                parseLocationResult(response)
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.e("Error", "onErrorResponse: Error= $error")
                    Log.e("Error", "onErrorResponse: Error= " + error.message)
                }
            })
        App.instance?.getRequestQueue()?.add(request)
    }

    private fun parseLocationResult(response: JSONObject?) {
        arraylist = ArrayList()
        val obj = response?.getJSONArray("predictions")
        // App.lati = obj?.getJSONObject(0)?.getJSONObject("geometry")?.getJSONObject("location")?.getString("lat")?.toDouble()!!
        //App.longi = obj?.getJSONObject(0)?.getJSONObject("geometry")?.getJSONObject("location")?.getString("lng")?.toDouble()!!
        for (i in 0 until obj?.length()!!) {
            val name = obj.getJSONObject(i).getString("description")
            //val formatted_address = obj.getJSONObject(i).getString("formatted_address")
            val place_id = obj.getJSONObject(i)?.getString("place_id")

            val url = ""//obj.getJSONObject(i)?.getString("icon")
            url?.let { place_id?.let { it1 -> ModelPlaceSearch(name, it, it1, "") } }
                ?.let { arraylist!!.add(it) }
        }

        val adapter = SearchLocationAdapter(arraylist!!, this)
        rev?.layoutManager = LinearLayoutManager(this)
        rev?.adapter = adapter

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onPause() {
        super.onPause()
    }

}