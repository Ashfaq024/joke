package com.example.joke.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.joke.LoadingState
import com.example.joke.LoadingState.*
import com.example.joke.R
import com.example.joke.databinding.ActivityRegisterBinding
import com.example.joke.model.UserModel
import com.example.joke.viewmodel.UserViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import org.koin.android.ext.android.inject
import java.util.*


class SignupActivity : AppCompatActivity() {
    private val viewModel: UserViewModel by inject()
    private var binding: ActivityRegisterBinding? = null
    private var mAuth: FirebaseAuth? = null
    var mDatabase: DatabaseReference? = null
    var checked = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        mDatabase = FirebaseDatabase.getInstance().reference
        mAuth = FirebaseAuth.getInstance()
        binding!!.viewModel = viewModel
        val usermodel = UserModel(Loadingstates = LoadingState.LOADING)
       binding!!.userModel = usermodel
        binding!!.executePendingBindings()
        binding!!.lifecycleOwner = this
        viewModel.data.observe(::getLifecycle) {

            if (it.status == true) {
                launchHomeScreen()
            }
        }

        customTextView()
        customLoginTextView()
        binding!!.agree.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                checked = isChecked
            }
        }


    }


    private fun launchHomeScreen() {
        this@SignupActivity.startActivity(
            Intent(this@SignupActivity, MainActivity::class.java)
        )
        this@SignupActivity.finish()
    }

    private fun customLoginTextView() {
        val spanTxt = SpannableStringBuilder(
            "Already have and account? "
        )
        spanTxt.append("Login")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                finish()
            }
        }, spanTxt.length - "Login".length, spanTxt.length, 0)
        binding!!.login!!.movementMethod = LinkMovementMethod.getInstance()
        binding!!.login.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }

    private fun customTextView() {
        val spanTxt = SpannableStringBuilder(
            "I agree to the "
        )
        spanTxt.append("Term of services")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {}
        }, spanTxt.length - "Term of services".length, spanTxt.length, 0)
        spanTxt.append(" and")
        spanTxt.setSpan(ForegroundColorSpan(Color.BLACK), 32, spanTxt.length, 0)
        spanTxt.append(" Privacy Policy")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {}
        }, spanTxt.length - " Privacy Policy".length, spanTxt.length, 0)
        binding!!.agreeText!!.movementMethod = LinkMovementMethod.getInstance()
        binding!!.agreeText.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }
}