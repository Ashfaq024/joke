package com.example.joke.activities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Binder
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class BackgroundLocationService : Service() {
    private var storageReference: StorageReference? = null
    private var mDatabase: DatabaseReference? = null
    private val binder = LocationServiceBinder()
    private var mLocationListener: LocationListener? = null
    private var mLocationManager: LocationManager? = null
    private val notificationManager: NotificationManager? = null
    private var locationRepository: LocationRepository? = null

    private val LOCATION_INTERVAL = 100f
    private val LOCATION_DISTANCE = 0.1f
    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    private inner class LocationListener(provider: String?) : android.location.LocationListener {
        private val lastLocation: Location? = null
        private val TAG = "LocationListener"
        private var mLastLocation: Location
        override fun onLocationChanged(location: Location) {
            mLastLocation = location
            Log.i(TAG, "LocationChanged: $location")
            mDatabase?.child("Location")?.child("Lat")?.setValue(location.latitude)
            mDatabase?.child("Location")?.child("Lng")?.setValue(location.longitude)
            // Save to local DB
            /* if (locationRepository != null) {
                 val loc = MyLocation()
                 loc.latitude = location.latitude
                 loc.longitude = location.longitude
                 locationRepository!!.insertLocation(loc)

                 mDatabase?.child("Location")?.child("Lat")?.setValue(location.latitude)
                 mDatabase?.child("Location")?.child("Lng")?.setValue(location.longitude)
             }*/
            Toast.makeText(
                this@BackgroundLocationService, """LAT: ${location.latitude}
 LONG: ${location.longitude}""", Toast.LENGTH_SHORT
            ).show()
        }

        override fun onProviderDisabled(provider: String) {
            Log.e(TAG, "onProviderDisabled: $provider")
        }

        override fun onProviderEnabled(provider: String) {
            Log.e(TAG, "onProviderEnabled: $provider")
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.e(TAG, "onStatusChanged: $status")
        }

        init {
            mLastLocation = Location(provider)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_NOT_STICKY
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onCreate() {

        //   locationRepository = LocationRepository(applicationContext)
        startForeground(12345678, notification)
        mDatabase = FirebaseDatabase.getInstance().reference
        val storage =
            FirebaseStorage.getInstance().getReferenceFromUrl("gs://joke-99612.appspot.com")
        storageReference = storage.root
        mDatabase = FirebaseDatabase.getInstance().reference
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mLocationManager != null) {
            try {
                mLocationManager!!.removeUpdates(mLocationListener!!)
                locationRepository = null
            } catch (ex: Exception) {
                // Log.i(TAG, "fail to remove location listeners, ignore", ex)
            }
        }
    }

    private fun initializeLocationManager() {
        if (mLocationManager == null) {
            mLocationManager =
                applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
        }
    }

    fun startTracking() {
        initializeLocationManager()
        mLocationListener = LocationListener(LocationManager.NETWORK_PROVIDER)
        try {
            mLocationManager!!.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                LOCATION_INTERVAL.toLong(),
                LOCATION_DISTANCE.toFloat(),
                mLocationListener!!
            )
        } catch (ex: SecurityException) {
            // Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (ex: IllegalArgumentException) {
            // Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    fun stopTracking() {
        onDestroy()
    }

    @get:RequiresApi(api = Build.VERSION_CODES.O)
    private val notification: Notification
        private get() {
            val channel = NotificationChannel(
                "channel_01",
                "My Channel",
                NotificationManager.IMPORTANCE_HIGH

            )
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            channel.enableVibration(true)
            channel.enableLights(true);
            channel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )

            notificationManager.createNotificationChannel(channel)
            val builder = Notification.Builder(applicationContext, "channel_01").setAutoCancel(true)
            return builder.build()
        }

    inner class LocationServiceBinder : Binder() {
        val service: BackgroundLocationService
            get() = this@BackgroundLocationService
    }

    companion object {
        private const val TAG = "BackgroundLocationService"
    }
}