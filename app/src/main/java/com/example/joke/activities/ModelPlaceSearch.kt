package com.example.joke.activities

data class ModelPlaceSearch(
    var name: String,
    var url: String,
    var placeId: String,
    var formatted_address: String
)