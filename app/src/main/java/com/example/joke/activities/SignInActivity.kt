package com.example.joke.activities

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.joke.R
import com.example.joke.databinding.ActivityLoginBinding
import com.example.joke.model.UserModel
import com.example.joke.utils.SharedPrefs.settingDone
import com.example.joke.utils.Utility.hideProgressBar
import com.example.joke.viewmodel.LogInViewModel
import com.example.joke.viewmodel.UserViewModel
import com.google.android.gms.auth.api.signin.*
import com.google.firebase.database.*
import org.koin.android.ext.android.inject
import java.util.*


class SignInActivity : AppCompatActivity() {
    private var binding: ActivityLoginBinding? = null
    private val viewModel: LogInViewModel by inject()

    private fun launchHomeScreen() {
        if (settingDone!!.isNotEmpty()) {
            startActivity(Intent(this@SignInActivity, LocationTracking::class.java))
        } else {
            startActivity(Intent(this@SignInActivity, SignInActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding!!.viewModel = viewModel
        //val usermodel = UserModel()
      //  binding!!.userModel = usermodel
        binding!!.executePendingBindings()
        binding!!.lifecycleOwner = this
        viewModel.getData().observe(::getLifecycle) {
            if (it != null) {
                if (it.status == true) {
                    launchHomeScreen()
                }

            } else {
                Toast.makeText(
                    this,
                    "Network not available ,please try again latter",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
            /*viewModel.data.observe(::getLifecycle) {

                if (it != null) {
                    if (!it.isNullOrEmpty()) {
                        launchHomeScreen()
                    }

                } else {
                    Toast.makeText(
                        this,
                        "Network not available ,please try again latter",
                        Toast.LENGTH_SHORT
                    ).show()
                }*/


            //customLoginTextView()

        }

        /* private fun customLoginTextView() {
             val spanTxt = SpannableStringBuilder(
                 "Dont have an account? "
             )
             spanTxt.append("Signup here")
             spanTxt.setSpan(object : ClickableSpan() {
                 override fun onClick(widget: View) {
                     startActivity(Intent(this@SignInActivity, SignupActivity::class.java))
                 }
             }, spanTxt.length - "Signup here".length, spanTxt.length, 0)
             signup!!.movementMethod = LinkMovementMethod.getInstance()
             signup.setText(spanTxt, TextView.BufferType.SPANNABLE)
         }*/


    }