package com.example.joke.interfaces

import androidx.lifecycle.MutableLiveData
import com.example.joke.model.UserModel
import kotlinx.coroutines.Job

interface SignUpCallback {
    fun ApiResponse(
        query: Map<String, String>,
        job: Job
    ): MutableLiveData<Result<UserModel>>
}