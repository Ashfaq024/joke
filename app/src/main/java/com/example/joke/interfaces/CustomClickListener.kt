package com.example.joke.interfaces

import android.view.View
import com.example.joke.model.PostModel

interface CustomClickListener {

 public fun cardClicked(model: PostModel?, position:Int)
 public fun onSharedIntent(model: PostModel?,position:Int)

}