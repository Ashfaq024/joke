package com.example.joke.interfaces

import androidx.lifecycle.MutableLiveData
import com.example.joke.model.UserModel

interface Callback {
    fun onSuccess(volumesResponseLiveData: MutableLiveData<UserModel?>)
}