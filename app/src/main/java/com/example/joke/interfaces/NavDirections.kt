package com.example.joke.interfaces

import android.os.Bundle
import androidx.annotation.IdRes

interface NavDirections {
    @get:IdRes
    val actionId: Int
    val arguments: Bundle
}
