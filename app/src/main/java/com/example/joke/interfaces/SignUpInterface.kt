package com.example.joke.interfaces

import com.example.joke.model.UserModel

interface SignUpInterface {
    suspend fun getResponse(params: Map<String, String>): UserModel
}