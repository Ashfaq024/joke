package com.example.joke.interfaces

import androidx.lifecycle.MutableLiveData
import com.example.joke.model.UserModel

interface UserRepository {
    fun LoginWithFirebase(query: UserModel)
    fun SignUpWithFirebase(query: UserModel)
    fun getVolumesResponseLiveData():MutableLiveData<UserModel>
}