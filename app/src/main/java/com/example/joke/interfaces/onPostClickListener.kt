package com.example.joke.interfaces

import android.view.View
import com.example.joke.model.PostModel

interface onPostClickListener {

  fun cardClicked(model: PostModel?, position:Int)
  fun onSharedIntent(model: PostModel?,position:Int)

}