package com.example.joke.interfaces

import android.os.Bundle
import androidx.appcompat.widget.AppCompatEditText
import com.example.joke.model.ErrorDescription

interface BaseView {
    fun setSoftInputMode(mode: Int)
    fun resetSoftInputMode()
    fun showKeyboard(editText: AppCompatEditText)
    fun hideKeyboard()
    fun noConnectivity()
    fun loaderVisibility(visibility: Boolean)
    fun showToast(message: String?)
    fun onApiError(errorDescription: ErrorDescription)
    //fun navigateToDestination(direction: NavDirections)
    fun navigateToDestination(id: Int, args: Bundle)
    fun getNavHostId(): Int?
    fun getLayoutId(): Int
}
