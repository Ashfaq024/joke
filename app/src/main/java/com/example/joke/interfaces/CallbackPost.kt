package com.example.joke.interfaces

interface CallbackPost {
 fun onChooseImage()
 fun onPostClick()
}