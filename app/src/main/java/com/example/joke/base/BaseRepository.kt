package com.example.joke.base

import androidx.lifecycle.*
import com.bumptech.glide.load.HttpException
import com.example.joke.model.ErrorDescription
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import com.example.joke.api.Result
import com.example.joke.model.UserModel

import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class BaseRepository<Response, Params> internal constructor() {
    abstract var showProgress: Boolean

    abstract suspend fun fetchFromNetwork(params: MutableLiveData<List<UserModel>>): MutableLiveData<List<UserModel>>
    private val result: MediatorLiveData<Result<Response>> = MediatorLiveData()


    open fun start(
        params: MutableLiveData<List<UserModel>>,
        job: Job

    ): LiveData<Result<out Any?>> = liveData(Dispatchers.IO + job) {
        emit(Result.Loading(showProgress))
        try {
            emit(Result.Success(fetchFromNetwork(params)))
        } catch (e: HttpException) {
            emit(Result.NetworkError<Error>(ErrorDescription(e.localizedMessage!!)))

        } catch (e: UnknownHostException) {
            emit(Result.NetworkError<Error>(ErrorDescription(e.localizedMessage!!)))
        } catch (e: SocketTimeoutException) {
            emit(Result.NetworkError<Error>(ErrorDescription(e.localizedMessage!!)))
        } catch (ex: Exception) {
            emit(Result.Failure<Error>(ErrorDescription(ex.localizedMessage!!)))
        }
    }
}