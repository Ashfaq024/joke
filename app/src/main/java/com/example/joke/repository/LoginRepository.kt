package com.example.joke.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.joke.App
import com.example.joke.utils.Utility.hideProgressBar
import com.example.joke.utils.Utility.showProgressBar
import com.example.joke.model.UserModel
import com.example.joke.utils.CommonUtils
import com.example.joke.utils.SharedPrefs


object LoginRepository {
    val volumesResponseLiveData: MutableLiveData<UserModel?> =
        MutableLiveData<UserModel?>()

    fun SignInwithEmail(
        context: Context,
        usermodel: UserModel
    ) {
        usermodel.username?.let {
            it
            usermodel.password?.let { it1 ->
                App.instance?.mAuth!!.signInWithEmailAndPassword(it, it1)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            usermodel.uid = App.instance?.mAuth!!.uid
                            usermodel.username = it
                            volumesResponseLiveData.postValue(usermodel)
                            CommonUtils.showToast("Logged in Successfully")
                            SharedPrefs.settingDone = App.instance?.mAuth!!.uid
                            SharedPrefs.userName = it

                        } else {
                            volumesResponseLiveData.postValue(usermodel)
                            CommonUtils.showToast("Authentication Failed!")
                            context.hideProgressBar()
                        }


                    }
            }


        }

    }

    fun getVolumesResponseLiveData(): LiveData<UserModel?> {
        return volumesResponseLiveData
    }
}



