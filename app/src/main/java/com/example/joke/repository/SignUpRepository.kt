package com.example.joke.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.joke.App
import com.example.joke.LoadingState
import com.example.joke.interfaces.UserRepository
import com.example.joke.model.UserModel
import com.example.joke.utils.CommonUtils
import com.example.joke.utils.SharedPrefs
import com.example.joke.utils.Utility.hideProgressBar

class SignUpRepository(var context: Context) : UserRepository {
    val response: MutableLiveData<UserModel> = MutableLiveData<UserModel>()
    var userModel: UserModel? = null
    override fun LoginWithFirebase(query: UserModel) {
        App.instance?.mAuth!!.createUserWithEmailAndPassword(
            query.email.toString(),
            query.password.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    userModel =
                        UserModel(
                            App.instance?.mAuth?.uid.toString(),
                            query.name,
                            query.username,
                            query.password,
                            query.email,
                            System.currentTimeMillis(),
                            true,
                            LoadingState.LOADED
                        )
                    response.postValue(userModel)
                    SharedPrefs.userModel = userModel
                    App.instance?.mDatabase!!.child("users")
                        .child(App.instance?.mAuth?.uid.toString())
                        .setValue(userModel).addOnSuccessListener {
                            SharedPrefs.userModel = userModel
                            context.hideProgressBar()
                            CommonUtils.showToast("Successfully Registered")
                        }
                } else {
                    response.postValue(null)

                }
            }
        //  return response;
    }

    override fun SignUpWithFirebase(query: UserModel) {
        userModel?.Loadingstates = LoadingState.LOADING
        App.instance?.mAuth!!.createUserWithEmailAndPassword(
            query.email.toString(),
            query.password.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    userModel = UserModel(
                        App.instance?.mAuth?.uid.toString(),
                        query.name,
                        query.username,
                        query.password,
                        query.email,
                        System.currentTimeMillis(),
                        true, LoadingState.LOADED
                    )

                    response.postValue(userModel)

                    SharedPrefs.userModel = userModel
                    App.instance?.mDatabase!!.child("users")
                        .child(App.instance?.mAuth?.uid.toString())
                        .setValue(userModel).addOnSuccessListener {
                            SharedPrefs.userModel = userModel
                        }
                } else {
                    userModel?.Loadingstates = LoadingState.error("User Already Taken")
                    response.postValue(userModel)

                }
            }

    }

    override fun getVolumesResponseLiveData(): MutableLiveData<UserModel> {
        return response
    }
}



