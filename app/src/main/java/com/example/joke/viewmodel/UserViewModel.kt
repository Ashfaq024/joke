package com.example.joke.viewmodel

import android.content.Context
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.joke.LoadingState
import com.example.joke.interfaces.UserRepository
import com.example.joke.model.FormErrors
import com.example.joke.model.UserModel
import com.example.joke.utils.Utility.isInternetAvailable
import com.example.joke.utils.Utility.showProgressBar

class UserViewModel(
    var context: Context, private val repository: UserRepository
) : ViewModel() {

    val formErrors = ObservableArrayList<FormErrors>()
    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState
    val data = repository.getVolumesResponseLiveData()

    fun onSignUpClicked(usermodel: UserModel?) {
        // if (!performValidation()) {
        if (context.isInternetAvailable()) {
            context.showProgressBar()
            usermodel?.let {
                repository.SignUpWithFirebase(
                    it
                )
            }
        } else {

            usermodel?.status = false
        }

    }

    fun onClicked(usermodel: UserModel?) {
        if (context.isInternetAvailable()) {

            usermodel?.let {
                repository.LoginWithFirebase(it)
            }
        } else {
            usermodel?.status = false
        }
    }

}
