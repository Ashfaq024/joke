package com.example.joke.bindings

import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

class ViewBindings {
    companion object {
        @BindingAdapter("app:errorText")
        @JvmStatic
        fun setErrorMessage(view: TextInputLayout, errorMessage: String) {
            view.error = errorMessage
        }

        @BindingAdapter("app:onClick")
        @JvmStatic
        fun setOnClick(view: View, clickListener: View.OnClickListener?) {
            view.setOnClickListener(clickListener)
            view.isClickable = true
        }
    }

}