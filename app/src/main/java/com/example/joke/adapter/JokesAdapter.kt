package com.example.joke.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.joke.BR
import com.example.joke.R
import com.example.joke.databinding.ItemRowBinding
import com.example.joke.interfaces.CustomClickListener
import com.example.joke.interfaces.onPostClickListener
import com.example.joke.model.PostModel


class JokesAdapter(
    private val items: MutableList<PostModel>,
    var context: Context,
    var postClickListener: onPostClickListener
) :
    RecyclerView.Adapter<JokesAdapter.ViewHolder>(), CustomClickListener {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemRowBinding = ItemRowBinding.inflate(inflater, parent, false)
        binding.itemClickListener = this
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel: PostModel = items[position]
        holder.bind(dataModel)

    }

    inner class ViewHolder(val binding: ItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PostModel) {
            binding.setVariable(BR.postModel, item)

            binding.executePendingBindings()
        }
    }

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl", "progressbar")
        fun loadImage(view: ImageView, url: String?, progressbar: ProgressBar) {
            Glide.with(view.context).load(url)
                .apply(RequestOptions().override(200, 200))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        view.setImageResource(R.drawable.placeholder)
                        progressbar.visibility = View.GONE
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        view.setImageDrawable(resource)
                        progressbar.visibility = View.GONE
                        return true
                    }

                }).into(view)


        }


        @BindingAdapter("android:src")
        @JvmStatic
        fun setImageViewResource(imageView: ImageView, resource: Int) {
            imageView.setImageResource(resource)
        }

    }

    override fun cardClicked(model: PostModel?, position: Int) {
        postClickListener?.cardClicked(model, position)
    }

    override fun onSharedIntent(model: PostModel?, position: Int) {
        postClickListener?.onSharedIntent(model, position)
    }


}







