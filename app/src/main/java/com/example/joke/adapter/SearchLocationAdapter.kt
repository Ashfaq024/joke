package com.example.joke.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.joke.App
import com.example.joke.R
import com.example.joke.activities.LocationTracking
import com.example.joke.activities.ModelPlaceSearch


internal class SearchLocationAdapter(
    private val arrayList: ArrayList<ModelPlaceSearch>,
    private val context: Context,
) : RecyclerView.Adapter<SearchLocationAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchLocationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.searchlocation, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: SearchLocationAdapter.ViewHolder, position: Int) {
        holder.bindItems(arrayList[position])
        holder.itemView.setOnClickListener {
            if (App.isDropoff && !App.isPickup) {
                val intent = Intent(context, LocationTracking::class.java)
                intent.putExtra("pickUpCityID", arrayList[position].placeId)
                intent.putExtra("pickUpLocationAddress", arrayList[position].name)
                ((context) as Activity).setResult(10, intent);
                (context).finish()
            } else {
                val intent = Intent(context, LocationTracking::class.java)
                intent.putExtra("pickUpCityID", arrayList[position].placeId)
                intent.putExtra("pickUpLocationAddress", arrayList[position].name)
                ((context) as Activity).setResult(11, intent);
                (context).finish()
            }
        }

    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageViewOptionIcon: ImageView
        var name: TextView
        var formattedAddress: TextView

        init {
            name = itemView.findViewById(R.id.name)
            imageViewOptionIcon = itemView.findViewById(R.id.icon)
            formattedAddress = itemView.findViewById(R.id.formattedAddress)
        }

        fun bindItems(items: ModelPlaceSearch) {
            name.text = items.name
            formattedAddress.text = items.formatted_address
            //   Glide.with(context).load(items.url).into(imageViewOptionIcon)
        }
    }
}