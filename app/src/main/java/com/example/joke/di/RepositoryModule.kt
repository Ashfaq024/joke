package com.example.joke.di

import android.content.Context
import com.example.joke.api.AllNewApi
import com.example.joke.interfaces.UserRepository
import com.example.joke.repository.SignUpRepository
import com.example.joke.viewmodel.LogInViewModel
import com.example.joke.viewmodel.UserViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module



val viewModelModule = module {
    viewModel { UserViewModel(androidContext(),get()) }

}

val repositoryModule = module {
    single {
        createSignUpRepository(androidContext())
    }
}
fun createSignUpRepository(context: Context): UserRepository {
    return SignUpRepository(context)
}
/*val sharedPrefModule = module {
    single { createMasterKey(androidContext()) }
    single { creteEncryptedSharedPref(androidContext(), get()) }
    single { EncryptSharedPref(get()) }
}*/
/*fun createMasterKey(context: Context): MasterKey {
    return MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build();
}

private fun creteEncryptedSharedPref(
    context: Context,
    masterKeyAlias: MasterKey
): SharedPreferences {

    return EncryptedSharedPreferences.create(
        context,
        KEY_PREF_NAME,
        masterKeyAlias,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
}*/




