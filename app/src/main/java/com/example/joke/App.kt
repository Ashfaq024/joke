package com.example.joke

import android.app.Application
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.IBinder
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.example.joke.activities.BackgroundLocationService
import com.example.joke.activities.BackgroundLocationService.LocationServiceBinder
import com.example.joke.activities.MyService
import com.example.joke.di.repositoryModule
import com.example.joke.di.viewModelModule
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.teliver.sdk.core.Teliver.startTracking
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class App : Application(), LocationListener {
    private var storageReference: StorageReference?=null
    private var gpsService: BackgroundLocationService? = null
    private var locationManager: LocationManager? = null
    var mDatabase: DatabaseReference? = null
    var mAuth: FirebaseAuth? = null
    private var mRequestQueue: RequestQueue? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this)

        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(viewModelModule + repositoryModule)
        }
        mDatabase = FirebaseDatabase.getInstance().reference
        val storage =
            FirebaseStorage.getInstance().getReferenceFromUrl("gs://joke-99612.appspot.com")
        storageReference = storage.root
        mDatabase = FirebaseDatabase.getInstance().reference
       /* mDatabase?.child("Location")?.child("Lat")?.setValue(lati)
        mDatabase?.child("Location")?.child("Lng")?.setValue(longi)*/

    }


    companion object {

        var isDropoff: Boolean = false
        var isPickup: Boolean = false
        var lati: Double = 31.234455
        var longi: Double = 74.489920

        @JvmStatic
        var instance: App? = null
            private set
    }

    override fun onLocationChanged(p0: Location) {
        lati = p0.latitude
        longi = p0.longitude
    }


    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(applicationContext)
        }
        return mRequestQueue
    }


}