package com.example.joke.api

import android.service.autofill.UserData
import androidx.lifecycle.LiveData
import com.example.joke.model.UserModel
import com.example.joke.repository.LoginRepository.volumesResponseLiveData


class AllNewApi {
    fun getVolumesResponseLiveData(): LiveData<UserModel?>? {
        return volumesResponseLiveData
    }
}
