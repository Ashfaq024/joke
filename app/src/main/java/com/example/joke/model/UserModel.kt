package com.example.joke.model

import com.example.joke.LoadingState


data class UserModel @JvmOverloads constructor(
    var uid: String? = "",
    var username: String? = "",
    var name: String? = "",
    var password: String? = "",
    var email: String? = "",
    var time: Long = 0,
    var status: Boolean? = false,
    var Loadingstates: LoadingState
)


