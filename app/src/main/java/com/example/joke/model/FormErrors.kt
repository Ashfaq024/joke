package com.example.joke.model

enum class FormErrors {
    MISSING_NAME,
    INVALID_EMAIL,
    INVALID_PASSWORD,
    PASSWORDS_NOT_MATCHING,
}