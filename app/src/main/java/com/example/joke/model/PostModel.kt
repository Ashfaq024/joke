package com.example.joke.model

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

data class PostModel @JvmOverloads constructor(
    var uid: String? = null,
    var Joketitle: String? = null,
    var JokeDescription: String? = null,
    var Imagepath: String? = null,
    var postTime: String? = null,
    var userName: String? = null,
    var postID: String = "",
    var isfevorit: Boolean = false,
    var upvote: Int? =0,
    ) {
    @SuppressLint("SimpleDateFormat")
    fun getDate(milliSeconds: String): String? {
        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds.toLong()
        return formatter.format(calendar.time)
    }
}


