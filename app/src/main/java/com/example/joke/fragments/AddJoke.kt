package com.example.joke.fragments

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.example.joke.interfaces.CallbackPost
import com.example.joke.R
import com.example.joke.databinding.FragmentNewJokeBinding
import com.example.joke.model.PostModel
import com.example.joke.utils.CommonUtils
import com.example.joke.utils.SharedPrefs
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.ByteArrayOutputStream
import java.io.IOException


class AddJoke : Fragment()/*, CallbackPost*/ {
    private lateinit var binding: FragmentNewJokeBinding
    private var imageLink: String? = ""
    var mDatabase: DatabaseReference? = null
    private val PICK_IMAGE_REQUEST = 100
    private val REQUEST_CODE_PERMISSIONS_STORAGE = 101
    private var filePath: Uri? = null
    var storageReference: StorageReference? = null
    var imageData: ByteArray? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewJokeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.executePendingBindings()
        //binding.callback = this
        mDatabase = FirebaseDatabase.getInstance().reference
        val storage =
            FirebaseStorage.getInstance().getReferenceFromUrl("gs://joke-99612.appspot.com")
        storageReference = storage.root

    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults:
        IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS_STORAGE) {
            choosImage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == AppCompatActivity.RESULT_OK && data != null && data.data != null) {
            filePath = data.data
            try {
                val bitmap1 =
                    MediaStore.Images.Media.getBitmap(
                        requireContext().contentResolver,
                        filePath
                    )
                val baos = ByteArrayOutputStream()
                bitmap1!!.compress(Bitmap.CompressFormat.JPEG, 25, baos)
                imageData = baos.toByteArray()
                //profile_image.setImageBitmap(bitmap1)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun uploadImage(filePath: Uri?, data: ByteArray) {
        imageLink = "noUrl"
        if (filePath != null) {
            val progressDialog = ProgressDialog(requireContext())
            progressDialog.setTitle("Uploading...")
            progressDialog.show()
            storageReference = storageReference!!.child(
                "images/" + SharedPrefs.userModel?.uid.toString()
            )
            storageReference?.child(mDatabase?.push()?.key!!)?.putBytes(data)
                ?.addOnSuccessListener {
                    val result = it.metadata!!.reference!!.downloadUrl;
                    result.addOnSuccessListener {

                        imageLink = it.toString()

                        progressDialog.setCanceledOnTouchOutside(false)
                        progressDialog.dismiss()
                     //   PostJokeNow()
                    }

                }
                ?.addOnFailureListener { e ->
                    progressDialog.dismiss()
                    Toast.makeText(requireContext(), "Failed " + e.message, Toast.LENGTH_SHORT)
                        .show()
                }
                ?.addOnProgressListener { taskSnapshot ->
                    val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot
                        .totalByteCount
                    progressDialog.setMessage("Uploaded " + progress.toInt() + "%")
                }
        }
    }

    /*override fun onChooseImage() {
        if (
            ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            choosImage()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_CODE_PERMISSIONS_STORAGE
            )
        }
    }*/

    private fun choosImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra("crop", "true")
        intent.putExtra("scale", true)
        intent.putExtra("aspectX", 25)
        intent.putExtra("aspectY", 20)
        startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            PICK_IMAGE_REQUEST
        )
    }

   /* override fun onPostClick() {
        if (FirebaseAuth.getInstance().currentUser == null || FirebaseAuth.getInstance().currentUser?.uid == null) {
            Toast.makeText(activity, "Unable to post joke.", Toast.LENGTH_LONG).show()
        } else if (jock_title!!.text.isNullOrEmpty()) {
            Toast.makeText(activity, "Please add joke title.", Toast.LENGTH_LONG).show()
        } else if (jock_desc!!.text.isNullOrEmpty()) {
            Toast.makeText(activity, "Please add joke title.", Toast.LENGTH_LONG).show()
        } else if (filePath == null) {
            Toast.makeText(activity, "Please select some image", Toast.LENGTH_LONG).show()
        } else {
            uploadImage(filePath, imageData!!)
        }
    }
*/
    /*fun PostJokeNow() {
        val postModel = PostModel()
        FirebaseAuth.getInstance().currentUser?.uid?.let { it1 ->
            postModel.uid = FirebaseAuth.getInstance().currentUser?.uid
            postModel.Joketitle = jock_title!!.text.toString()
            postModel.JokeDescription = jock_desc!!.text.toString()
            postModel.Imagepath = imageLink
            postModel.postTime = System.currentTimeMillis().toString()
            postModel.userName = SharedPrefs.userName
            // postModel.isfevorit = true

            when {
                jock_title!!.text.isNullOrEmpty() -> {
                    jock_title.error = "Enter your joke title"
                    jock_title.requestFocus()
                    return
                }
                jock_desc!!.text.isNullOrEmpty() -> {
                    jock_desc.error = "Enter your joke title"
                    jock_desc.requestFocus()
                    return
                }
                else -> mDatabase!!.push().key?.let { it2 ->
                    progressBar1.visibility = View.VISIBLE

                    mDatabase!!.child("Jokes").child(it1).child(it2)
                        .setValue(postModel).addOnSuccessListener {
                            progressBar1.visibility = View.GONE
                            jock_title!!.text?.clear()
                            jock_desc!!.text?.clear()
                            filePath = null
                            imageData = null
                            profile_image.setImageResource(R.drawable.placeholder)
                            CommonUtils.showToast("Joke Successfully Posted.")
                        }
                }
            }
        }
    }*/


}