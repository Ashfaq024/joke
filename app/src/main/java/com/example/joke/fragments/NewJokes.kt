package com.example.joke.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.joke.BR
import com.example.joke.adapter.JokesAdapter
import com.example.joke.databinding.FragmentAddJokeBinding
import com.example.joke.interfaces.CustomClickListener
import com.example.joke.interfaces.onPostClickListener
import com.example.joke.model.PostModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class NewJokes : Fragment(){

    private lateinit var _binding: FragmentAddJokeBinding
    var selectedModel: PostModel? = null
    lateinit var postClickListener: onPostClickListener
    var jokeAdapter: JokesAdapter? = null

    var arrayList: MutableList<PostModel> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddJokeBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            this.postClickListener = context as onPostClickListener
        } catch (e: Exception) {
            Log.e("TAG", "onAttach: " + e.toString())

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ref = FirebaseDatabase.getInstance()
        with(_binding)
        {
            mProgressBar.visibility = View.VISIBLE
        }
        _binding.executePendingBindings()
        ref.getReference("Jokes").addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                arrayList = ArrayList()
                for (eachChild in snapshot.children) {
                    for (userChild in eachChild.children) {
                        val key = userChild.key
                        val joke: PostModel? =
                            userChild.getValue(PostModel::class.java)
                        if (key != null) {
                            joke?.postID = key.toString()
                        }
                        if (joke != null) {
                            arrayList.add(joke)
                        }
                    }
                }
                if (arrayList.size > 0) {
                    populateData(arrayList)
                } else {
                    with(_binding)
                    {
                        mProgressBar.visibility = View.GONE
                    }

                    Toast.makeText(activity, "No joke found", Toast.LENGTH_SHORT).show()
                }


            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    private fun populateData(dataModelList: MutableList<PostModel>) {

        _binding.setVariable(BR.postModel, dataModelList)

        jokeAdapter = JokesAdapter(dataModelList, requireContext(), postClickListener)
        _binding.jokesAdapter = jokeAdapter
        with(_binding)
        {
            mProgressBar.visibility = View.GONE
        }


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 99) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                shareWithFriends()

            }
        }
    }

    fun onSharedIntent(model: PostModel?, position: Int) {
        try {
            selectedModel = model
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                shareWithFriends()
            } else {
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    99
                )
            }


        } catch (e: Exception) {
            e.message
        }

    }


    private fun shareWithFriends() {
        Glide.with(requireActivity())
            .asBitmap()
            .load(selectedModel?.Imagepath)
            .into(object : SimpleTarget<Bitmap?>() {

                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap?>?
                ) {

                    val sharingIntent = Intent(Intent.ACTION_SEND)
                    sharingIntent.type = "image/*"
                    sharingIntent.putExtra(
                        Intent.EXTRA_STREAM,
                        resource?.let { getLocalBitmapUri(it, requireContext()) })

                    sharingIntent.putExtra(
                        Intent.EXTRA_TEXT,
                        selectedModel?.Joketitle + "\n" + selectedModel?.JokeDescription
                    );
                    startActivity(Intent.createChooser(sharingIntent, "Share Joke"))

                }
            })
    }


    fun getLocalBitmapUri(drawable: Bitmap, context: Context): Uri? {

        var bmpUri: Uri? = null
        var bmp: Bitmap? = drawable
        try {
            val file = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "share_image_" + System.currentTimeMillis() + ".png"
            )
            val out = FileOutputStream(file)
            bmp?.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }

    fun cardClicked(model: PostModel?, position: Int) {

        val ref = FirebaseDatabase.getInstance().reference
            .child("Jokes").child(FirebaseAuth.getInstance().currentUser?.uid.toString())
            .child(model?.postID.toString())
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {

                    val joke: PostModel? =
                        dataSnapshot.getValue(PostModel::class.java)
                    if (model?.isfevorit == true) {
                        ref.child("isfevorit").setValue(false)
                        ref.child("upvote").setValue(0)
                        model.isfevorit = false
                        model.upvote = 0
                    } else {
                        ref.child("isfevorit").setValue(true)
                        ref.child("upvote").setValue(1)
                        model?.isfevorit = true
                        model?.upvote = 1
                    }
                    jokeAdapter?.notifyDataSetChanged()
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }


}
