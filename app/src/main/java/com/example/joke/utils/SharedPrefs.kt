package com.example.joke.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.joke.App
import com.example.joke.model.UserModel
import com.google.gson.Gson

object SharedPrefs {
    @JvmStatic
    var userModel: UserModel?
        get() {
            val gson = Gson()
            return gson.fromJson(preferenceGetter("UserModel"), UserModel::class.java)
        }
        set(model) {
            val gson = Gson()
            val json = gson.toJson(model)
            preferenceSetter("UserModel", json)
        }

    @JvmStatic
    var settingDone: String?
        get() = preferenceGetter("setting")
        set(uid) {
            preferenceSetter("setting", uid)
        }
    @JvmStatic
    var userName: String?
        get() = preferenceGetter("user")
        set(uid) {
            preferenceSetter("user", uid)
        }


    fun preferenceSetter(key: String?, value: String?) {
        val pref = App.instance!!.applicationContext.getSharedPreferences(
            "user",
            Context.MODE_PRIVATE
        )
        val editor = pref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun preferenceGetter(key: String?): String? {
        val pref: SharedPreferences
        var value: String? = ""
        pref = App.instance!!.applicationContext.getSharedPreferences(
            "user",
            Context.MODE_PRIVATE
        )
        value = pref.getString(key, "")
        return value
    }


    fun logout() {
        val pref = App.instance!!.applicationContext.getSharedPreferences(
            "user",
            Context.MODE_PRIVATE
        )
        val editor = pref.edit()
        editor.clear()
        editor.apply()
    }
}