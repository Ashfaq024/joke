package com.example.joke.utils

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.example.joke.App.Companion.instance

object CommonUtils {
    fun showToast(msg: String?) {
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(
                instance!!.applicationContext,
                msg,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}